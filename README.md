# Практическая работа по модулю "Docker" курса "DevOps-инженер. Основы"

1. Устанавливаем `docker` и `docker-compose`

1. Клонируем репозиторий
   ```shell
   git clone https://gitlab.com/skillbox_edu/devops/basic/module_8.git
   cd module_8
   ```
   
1. Запускаем контейнеры
   ```shell
   sh startup.sh
   ```
   
   ![Запуск контейнеров](docs/imgs/docker-compose-up.png)
   
1. Проверяем, что все контейнеры запущены
   ```shell
   docker-compose ps
   ```
   
   ![Просмотр запущенных контейнеров](docs/imgs/docker-compose-ps.png)
